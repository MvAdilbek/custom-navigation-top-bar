//
//  ViewController.swift
//  TestLargeNavigationBar
//
//  Created by Adilbek Mailanov on 2/27/19.
//  Copyright © 2019 Adilbek Mailanov. All rights reserved.
//

import UIKit

class ViewController: UIViewController { // UITableViewController arkyly BOLMAIDY!!! Toka cerez UIViewController. This comment i write just for Askar (umnik)
    
    private let list = Data.list
    
    private let tableView = UITableView().apply { (tableView) in
        tableView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private let tableViewHeader = UIView().apply { (view) in
        view.backgroundColor = .red
    }
    
    private let tableViewHeaderBar = UIView().apply { (view) in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .green
    }
    
    private let button = UIButton().apply { (button) in
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .orange
        button.titleLabel?.text = "Askar chert"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        setupTableView()
        setupHeader()
        setupHeaderBar()
        setupButton()
    }
    
    private func setupTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellReuseIdendifier")
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(tableView)
        
        NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: tableView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: tableView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 0).isActive = true
        
        tableView.scrollIndicatorInsets.top = Dim.TableViewHeaderHeight
    }
    
    private func setupHeader() {
        tableViewHeader.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: Dim.TableViewHeaderHeight) // ETO TOZHE OBEZATELNO
        tableView.tableHeaderView = tableViewHeader
        tableView.bringSubviewToFront(tableViewHeader)
    }
    
    private func setupHeaderBar() {
        tableViewHeader.addSubview(tableViewHeaderBar)
        
        NSLayoutConstraint(item: tableViewHeaderBar, attribute: .width, relatedBy: .equal, toItem: tableViewHeader, attribute: .width, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: tableViewHeaderBar, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Dim.LargeBarHeight).isActive = true
        NSLayoutConstraint(item: tableViewHeaderBar, attribute: .bottom, relatedBy: .greaterThanOrEqual, toItem: view, attribute: .top, multiplier: 1, constant: Dim.SmallBarHeight).isActive = true
        NSLayoutConstraint(item: tableViewHeaderBar, attribute: .top, relatedBy: .equal, toItem: tableView, attribute: .top, multiplier: 1, constant: 0).apply { (this) in
            this.priority = UILayoutPriority(rawValue: 999)
            }.isActive = true
    }
    
    private func setupButton() {
        tableViewHeader.addSubview(button)
        
        NSLayoutConstraint(item: button, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 200).isActive = true
        NSLayoutConstraint(item: button, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50).isActive = true
        NSLayoutConstraint(item: button, attribute: .top, relatedBy: .equal, toItem: tableViewHeader, attribute: .top, multiplier: 1, constant: 170).apply { (ns) in
            ns.priority = UILayoutPriority(rawValue: 998)
            }.isActive = true
        NSLayoutConstraint(item: button, attribute: .centerY, relatedBy: .greaterThanOrEqual, toItem: tableViewHeaderBar, attribute: .bottom, multiplier: 1, constant: 0).apply { (ns) in
            ns.priority = UILayoutPriority(rawValue: 999)
            }.isActive = true
        NSLayoutConstraint(item: button, attribute: .centerX, relatedBy: .equal, toItem: tableViewHeader, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdendifier", for: indexPath)
        cell.textLabel?.text = "\(indexPath.row) - \(list[indexPath.row])"
        return cell
    }
    
    private class Dim {
        static let TableViewHeaderHeight: CGFloat = 250
        static let LargeBarHeight: CGFloat = 150
        static let SmallBarHeight: CGFloat = 70
    }
}
