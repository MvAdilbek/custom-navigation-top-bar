//
//  ViewController.swift
//  TestLargeNavigationBar
//
//  Created by Adilbek Mailanov on 2/27/19.
//  Copyright © 2019 Adilbek Mailanov. All rights reserved.
//

import UIKit
import Foundation

protocol HasApply { }

extension HasApply {
    func apply(closure:(Self) -> ()) -> Self {
        closure(self)
        return self
    }
}

extension NSObject: HasApply { }
